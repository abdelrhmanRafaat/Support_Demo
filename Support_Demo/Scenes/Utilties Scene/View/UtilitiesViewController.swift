//
//  UtilitiesViewController.swift
//  Support_Demo
//
//  Created by Macbook on 12/04/2021.
//

import UIKit

class UtilitiesViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Localization("setting_2")
        tableView.registerNIB(cell: UtilityCell.self)
        tableView.separatorStyle = .none
        tableView.rowHeight = 50
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue() as UtilityCell
        cell.isExpandFunc = {
            self.tableView.rowHeight = cell.isExpanded.isOn ? 100 : 50
            tableView.reloadData()
        }
        return cell
    }
}
