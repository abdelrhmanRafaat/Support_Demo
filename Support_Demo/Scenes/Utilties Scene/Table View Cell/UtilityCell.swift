//
//  UtilityCell.swift
//  Support_Demo
//
//  Created by Macbook on 12/04/2021.
//

import UIKit

class UtilityCell: UITableViewCell {
   @IBOutlet var isExpanded : UISwitch!
    var isExpandFunc: (()->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    @IBAction func SwitchFunc(_ sender: Any) {
        isExpandFunc?()
    }}
