//
//  SettingsTableViewCell.swift
//  Support_Demo
//
//  Created by Macbook on 10/04/2021.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    
    @IBOutlet var settingName : UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(index : Int) {
        settingName.text = Localization("setting_\(index)")
        accessoryType = .disclosureIndicator
    }
    func Go_to(index : Int) -> String {
        return "setting_\(index)"

    }

}
