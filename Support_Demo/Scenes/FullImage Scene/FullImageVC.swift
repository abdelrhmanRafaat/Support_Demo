//
//  FullImageVC.swift
//  Support_Demo
//
//  Created by Macbook on 07/04/2021.
//

import UIKit

class FullImageVC: UIViewController {
    @IBOutlet var image : UIImageView!
    var fullImage : UIImage!
    override func viewDidLoad() {
        super.viewDidLoad()
        image.image = fullImage
        // Do any additional setup after loading the view.
    }

}
