//
//  AppearanceVM.swift
//  Support_Demo
//
//  Created by Macbook on 10/04/2021.
//

import UIKit

class AppearanceVM : UIViewController {
    let defaults = UserDefaults.standard
    private var EngLang : Bool {
        if getLang() == "en" {
            return true
        }
        else {
            return false
        }}
    private var PrimaryIcon : Bool {
        let IconName = UIApplication.shared.alternateIconName ?? "Primary"
        if IconName == "Primary" {
            return true
        }
        else {
            return false
        }}
    private lazy var isDark = defaults.bool(forKey: "Dark_Mode")
    private lazy var Selected = [ [EngLang,!EngLang], [!isDark,isDark] , [PrimaryIcon,!PrimaryIcon] ]
    func SectionTitle(index : Int) -> String {
        return Localization("Appearance_\(index)")
    }
    private func Rowstitle(_ indexPath : IndexPath) -> String {
        return Localization("Appearance_\(indexPath.section)_\(indexPath.row)")
    }
    func DisplayCell(_ indexPath : IndexPath, tableView : UITableView, _ vm : AppearanceVM) -> UITableViewCell {
        let cell = tableView.dequeue() as AppearanceTableViewCell
        cell.configureCell(Rowstitle(indexPath), index: indexPath, vm)
        cell.selectionStyle = .none
        return cell
    }
    func Cell_Actions(_ indexpath : IndexPath, completion : @escaping () -> ()) {
        if Selected[indexpath.section][indexpath.row] {
            return
        }
        Selected[indexpath.section][0].toggle()
        Selected[indexpath.section][1].toggle()
        switch indexpath.section {
        case 0:
            if getLang() == "en" {
                LocalizationSystem.sharedInstance.setLanguage(languageCode: "ar")
                Reload()
            }
            else {
                LocalizationSystem.sharedInstance.setLanguage(languageCode: "en")
                Reload()
            }
        case 2:
            ChangeAppIcon()
        default:
            ChangeAppViewMode(isDark)
            isDark.toggle()
        }
        completion()
    }
    func SelectionState(_ indexPath : IndexPath) -> Bool {
        return Selected[indexPath.section][indexPath.row]
    }}
