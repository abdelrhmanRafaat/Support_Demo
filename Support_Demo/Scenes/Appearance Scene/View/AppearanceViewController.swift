//
//  AppearanceViewController.swift
//  Support_Demo
//
//  Created by Macbook on 10/04/2021.
//

import UIKit

class AppearanceViewController: UITableViewController {
    var appearanceVM : AppearanceVM!
    override func viewDidLoad() {
        super.viewDidLoad()
        appearanceVM = AppearanceVM()
        self.title = Localization("setting_0")
        tableView.separatorStyle = .none
        self.tableView.registerNIB(cell: AppearanceTableViewCell.self)
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 2
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50.0
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = appearanceVM.DisplayCell(indexPath, tableView: tableView, appearanceVM)
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 2 {
            return 55.0
        }
        return 50.0
    }
   override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
       let returnedView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width , height: 40.0)) //set these values as necessary
       returnedView.backgroundColor = .systemGray6
       let LangConstant : CGFloat = getLang() == "en" ? 1 : -1
       let label = UILabel(frame: CGRect(x: 10 * LangConstant, y: 15, width: view.frame.width , height: 20))
       label.text =  appearanceVM.SectionTitle(index: section)
        returnedView.addSubview(label)
                 return returnedView
           }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        appearanceVM.Cell_Actions(indexPath) {
            tableView.reloadData()
        }
    }
    
}
