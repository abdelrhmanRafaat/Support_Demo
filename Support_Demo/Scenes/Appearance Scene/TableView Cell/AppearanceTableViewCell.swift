//
//  AppearanceTableViewCell.swift
//  Support_Demo
//
//  Created by Macbook on 10/04/2021.
//

import UIKit

class AppearanceTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var TitleLbl: UILabel!
    @IBOutlet weak var App_Icon_image: UIImageView!
    @IBOutlet weak var SepratorLine: UIView!
    @IBOutlet weak var CheckMark: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        App_Icon_image.isHidden = true
        App_Icon_image.layer.borderWidth = 1.0
        App_Icon_image.layer.borderColor = UIColor.gray.cgColor
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
   private func CustomizeSepratorLine(_ index : Int) {
    if index == 1 {
        SepratorLine.isHidden = true
    }
    else {
        SepratorLine.isHidden = false
    }}
    private func AppIconView (_ index : Int, _ ImageName : String) {
        if index == 2 {
            App_Icon_image.isHidden = false
            TitleLbl.text = ""
            App_Icon_image.image = UIImage(named: ImageName)
        }}
    private func SelectionIcon (_ selection : Bool) {
        if selection {
            CheckMark.isHidden = false
        }
        else {
            CheckMark.isHidden = true
        }
    }
    func configureCell(_ name : String, index : IndexPath, _ appearanceVm : AppearanceVM) {
        TitleLbl.text = name
        AppIconView(index.section, name)
        SelectionIcon(appearanceVm.SelectionState(index))
        CustomizeSepratorLine(index.row)
    }
    
}
