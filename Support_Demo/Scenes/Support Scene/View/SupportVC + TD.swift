//
//  SupportVC + TD.swift
//  Support_Demo
//
//  Created by Macbook on 05/04/2021.
//

import UIKit


extension SupportViewController : UITableViewDelegate, UITableViewDataSource {
    func setupTableView()
    {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        height = tableView.heightAnchor.constraint(equalToConstant: 150.0)
        height?.isActive = true
        self.tableView.registerNIB(cell: SuggestCell.self)
        self.tableView.registerNIB(cell: AddattachmentsCell.self)
    }
    @IBAction func CollapseBtn(_sender : UIButton) {
        CustomizeTableView()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return supportvm.TableCellsNumber()
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = supportvm.displayTableViewCell(tableView: tableView, index: indexPath.row)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch supportvm.tableViewState() {
        case .colapse :
            if !KeyboardAppear {
              CustomizeTableView()
            }
        case .expand :
            SuggestCellActions(index: indexPath.row)
        case .none :
            SuggestCellActions(index: indexPath.row)
        }}
    
    
        func SuggestCellActions(index : Int) {
            switch index {
            case 0:
                print("")
            case 1:
                supportvm.AddImage(image: ScreenShot()) {
                    self.collectionView.reloadData()
                }
            case 2:
                PickImage()
            default:
                print("")
            }}
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
     }
}
