//
//  SupportViewController.swift
//  Support_Demo
//
//  Created by Macbook on 05/04/2021.
//

import UIKit

class SupportViewController: UIViewController {

    @IBOutlet weak var tableView : UITableView!
    @IBOutlet weak var Helplbl : UILabel!
    @IBOutlet weak var EmailtxtField : CustomTextField!
    @IBOutlet weak var TxtView: UITextView!
    @IBOutlet weak var collectionView : UICollectionView!
    @IBOutlet weak var CollapseView: UIView!
    @IBOutlet weak var SepratorView: UIView!
    @IBOutlet weak var FooterView: UIView!
    @IBOutlet weak var CollapseBtn: UIButton!
    @IBOutlet weak var PublishListBtn: UIButton!
    @IBOutlet weak var TxtViewPlaceHolder: UILabel!
    @IBOutlet weak var PoweredByLbl: UILabel!
    @IBOutlet weak var LogoImg: UIImageView!
    var KeyboardAppear : Bool = false
    var height : NSLayoutConstraint?
    var footBottom : NSLayoutConstraint?
    var sepratorBottom : NSLayoutConstraint?
    var PublishListBtnLeading : NSLayoutConstraint?
    var PublishListBtnTop : NSLayoutConstraint?
    var HelpLabelTrailing : NSLayoutConstraint?
    var tap : UITapGestureRecognizer!
    var supportvm : SupportVM!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureLocalization()
        setupSuggestRightBtn()
        configureView()
        setupTableView()
        setupCollectionView()
        changeLang()
        supportvm = SupportVM()
        let isDark = SettingsBundle.AppViewMode()
        ChangeAppViewMode(!isDark)
        // Do any additional setup after loading the view.
    }
}
