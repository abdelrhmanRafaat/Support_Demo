//
//  SupportVC + CD.swift
//  Support_Demo
//
//  Created by Macbook on 05/04/2021.
//

import UIKit

extension SupportViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func setupCollectionView()
    {
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.registerNIB(ImageCell.self)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return supportvm.imagesNumber()
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = supportvm.displayImageCell(index: indexPath, collectionView: collectionView)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80, height: 80 )
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! ImageCell
        self.performSegue(withIdentifier: "ShowFullImage", sender: cell)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowFullImage" {
            if let cell = sender as? ImageCell,
                let indexPath = self.collectionView.indexPath(for: cell) {
                let vc = segue.destination as! FullImageVC
                vc.fullImage = supportvm.getCurrentImage(indexPath.row)
            }}}
    
}
