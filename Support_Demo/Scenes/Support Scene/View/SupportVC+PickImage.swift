//
//  SupportVC+PickImage.swift
//  Support_Demo
//
//  Created by Macbook on 05/04/2021.
//

import Foundation
import UIKit


extension SupportViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func PickImage () {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.allowsEditing = false
            imagePicker.sourceType = .photoLibrary
            imagePicker.delegate = self
            self.present(imagePicker, animated: true, completion: nil)
        }}
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            self.dismiss(animated: true, completion: nil)
            supportvm.AddImage(image: selectedImage) {
                self.collectionView.reloadData()
            }
        }}
}
