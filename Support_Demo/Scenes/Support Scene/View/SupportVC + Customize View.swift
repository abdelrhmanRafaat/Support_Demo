//
//  SupportVC + Customize View.swift
//  Support_Demo
//
//  Created by Macbook on 05/04/2021.
//

import UIKit


extension SupportViewController {
    
    func configureLocalization () {
        EmailtxtField.placeholder = Localization("enter your email")
        TxtViewPlaceHolder.text = Localization("How can we improve your experience with this app?")
        PublishListBtn.setTitle(Localization("Edit the list"), for: .normal)
        Helplbl.text = Localization( "To help us solve the problem your report will include a list of steps to reproduce the problem.")
        PoweredByLbl.text = Localization("Powered by Instabug")
        self.title = Localization("Suggest an improvement")
    }
    func configureView() {
        Helplbl.numberOfLines = 0
        TxtViewPlaceHolder.numberOfLines = 0
        publishBtnConstrains()
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        tap = UITapGestureRecognizer(
        target: self,
        action: #selector(dismissMyKeyboard))
        NotificationCenter.default.addObserver(self, selector: #selector(handletxtinputChange), name: UITextView.textDidChangeNotification, object: nil)
        footBottom = FooterView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -18)
        sepratorBottom = SepratorView.bottomAnchor.constraint(equalTo: tableView.topAnchor, constant: -25)
        //publishBtnConstrains()
        footBottom?.isActive = true
        sepratorBottom?.isActive = true
        CollapseView.layer.cornerRadius = 2
        CollapseView.layer.masksToBounds = true
    }
    func collapseViewForKeyBoard(keyboardHeight : CGFloat) {
      supportvm.SetTableViewState()
       CustomizeTableView()
      footBottom?.constant = keyboardHeight
       sepratorBottom?.constant = 0
      footBottom?.isActive = true
      CollapseBtn.isEnabled = false
      view.addGestureRecognizer(tap)
    }
    func expandViewForKeyBoard() {
        supportvm.SetTableViewState()
        CustomizeTableView()
        footBottom?.constant = -18
        sepratorBottom?.constant = -25
        footBottom?.isActive = true
        CollapseBtn.isEnabled = true
        view.removeGestureRecognizer(tap)
    }
    func CustomizeTableView(){
        height?.constant = supportvm.TableViewHeight()
        TableViewAnimation()
        tableView.reloadData()
    }
    func publishBtnConstrains() {
        PublishListBtnLeading = PublishListBtn.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0.0)
        Helplbl.translatesAutoresizingMaskIntoConstraints = false
        PublishListBtnTop = PublishListBtn.topAnchor.constraint(equalTo: Helplbl.topAnchor, constant: 0.0)
        PublishListBtn.translatesAutoresizingMaskIntoConstraints = false
        let width = view.frame.width
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            switch getLang() {
            case "ar":
                switch getPhoneSize(width) {
                case .Small:
                    PublishListBtnLeading?.constant = 167
                    PublishListBtnLeading?.isActive = true
                case .Medium:
                    PublishListBtnLeading?.constant = 120
                    PublishListBtnLeading?.isActive = true
                case .Large:
                    PublishListBtnLeading?.constant = 87
                    PublishListBtnLeading?.isActive = true
                }
            case "en":
                switch getPhoneSize(width) {
                case .Small:
                    PublishListBtnLeading?.constant = 11
                    PublishListBtnLeading?.isActive = true
                    PublishListBtnTop?.constant = -45
                    PublishListBtnTop?.isActive = true
                case .Medium:
                    PublishListBtnLeading?.constant = 224
                    PublishListBtnLeading?.isActive = true
                case .Large:
                    PublishListBtnLeading?.constant = 208
                    PublishListBtnLeading?.isActive = true
                }
            default:
                print("")
            }
        case .pad:
            switch getLang() {
            case "ar":
                PublishListBtnLeading?.constant = 510
                PublishListBtnLeading?.isActive = true
            case "en":
                PublishListBtnLeading?.constant = 646
                PublishListBtnLeading?.isActive = true
            default:
                print("")
            }
        default:
            print("")
        }}
    @objc func dismissMyKeyboard(){
    //endEditing causes the view (or one of its embedded text fields) to resign the first responder status.
    //In short- Dismiss the active keyboard.
    view.endEditing(true)
    }
    @objc func handletxtinputChange() {
        TxtViewPlaceHolder.isHidden = !TxtView.text.isEmpty
    }
    @objc func handleKeyboardShow(notification: NSNotification) {
        KeyboardAppear = true
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height * -1
            collapseViewForKeyBoard(keyboardHeight: keyboardHeight)
            }}
    @objc func handleKeyboardHide() {
        KeyboardAppear = false
        expandViewForKeyBoard()
    }}


