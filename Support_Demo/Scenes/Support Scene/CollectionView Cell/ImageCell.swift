//
//  ImageCell.swift
//  ma5dom
//
//  Created by Macbook on 30/03/2021.
//

import UIKit

class ImageCell: UICollectionViewCell {
    
    var imageClosure: (() -> ())?
    
    @IBOutlet weak var clearBtn: UIButton!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var CircleView: UIView!
    @IBOutlet weak var AnimateImage: UIImageView!
    var img1 = UIImage()
    var img2 = UIImage()
    var img3 = UIImage()    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureView()
        // Initialization code
    }
    private func configureView () {
        img1 = UIImage(systemName: "magnifyingglass")!
        img2 = UIImage(systemName: "paintbrush.pointed")!
        img3 = UIImage(systemName: "square.split.2x2")!
        let arr = [img1, img2, img3]
        AnimateImage?.image = UIImage.animatedImage(with: arr, duration: 5.0)
        CircleView.layer.cornerRadius = 16
        CircleView.layer.masksToBounds = true
        image.layer.cornerRadius = 10
        image.layer.masksToBounds = true
        image.layer.borderWidth = 0.3
        image.layer.borderColor = UIColor.gray.cgColor
    }
    
    @IBAction func ClearBtn(_ sender: Any) {
        imageClosure?()
    }
    
}
