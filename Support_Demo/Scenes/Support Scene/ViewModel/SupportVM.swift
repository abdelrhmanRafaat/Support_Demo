//
//  SupportVM.swift
//  Support_Demo
//
//  Created by Macbook on 06/04/2021.
//

import UIKit

class SupportVM : UIViewController {
    private var toggle : Bool = true
    private var CellState : cellState?
    private var KeyboardAppear = false
    private var images : [UIImage] = [UIImage]()
    func AddImage(image : UIImage, completion : @escaping () -> ()) {
        self.images.append(image)
        completion()
    }
    func imagesNumber() -> Int {
        return images.count
    }
    func TableCellsNumber() -> Int {
        switch CellState {
        case .colapse:
            return 1
        case .expand:
            return 3
        case .none:
            return 3
        }}
   private func removeCurrentImage(index : Int){
        self.images.remove(at: index)
    }
    
    func displayImageCell(index : IndexPath, collectionView : UICollectionView) -> UICollectionViewCell {
        let cell = collectionView.dequeue(cell: ImageCell.self, for: index)
        let ImageData = images[index.row].jpegData(compressionQuality: 0.1)
        cell.image.image = UIImage(data: ImageData!)
        cell.imageClosure = {self.removeCurrentImage(index: index.row)
            collectionView.reloadData()
        }
        return cell
    }
    func getCurrentImage(_ index: Int) -> UIImage {
        return images[index]
    }
    func displayTableViewCell(tableView : UITableView, index : Int) -> UITableViewCell {
        switch CellState {
        case .colapse :
            let cell = tableView.dequeue() as AddattachmentsCell
            return cell
        case .expand :
            let cell = tableView.dequeue() as SuggestCell
            cell.configureView(index: index)
            return cell
        case .none :
            let cell = tableView.dequeue() as SuggestCell
            cell.configureView(index: index)
            return cell
        }}
    func TableViewHeight() -> CGFloat {
        toggle.toggle()
        self.CellState = self.toggle ? .expand : .colapse
        switch self.CellState {
        case .expand:
            return 150.0
            case .colapse:
            return 50.0
        case .none:
            return 150.0
         }}
    func tableViewState() -> cellState? {
        return CellState
    }
    func SetTableViewState(){
        toggle = true
    }
}
