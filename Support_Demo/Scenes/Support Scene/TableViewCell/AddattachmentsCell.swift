//
//  AddattachmentsCell.swift
//  ma5dom
//
//  Created by Macbook on 31/03/2021.
//

import UIKit

class AddattachmentsCell: UITableViewCell {
    @IBOutlet weak var attachmentLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        selectionStyle = .none
        attachmentLbl.text = Localization("Add attachments")
        // Configure the view for the selected state
    }
}
