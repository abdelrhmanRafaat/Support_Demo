//
//  SuggestCell.swift
//  ma5dom
//
//  Created by Macbook on 29/03/2021.
//

import UIKit

class SuggestCell: UITableViewCell {

    
    @IBOutlet weak var IconImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureView(index : Int) {
        IconImage.image = UIImage(systemName: Localization("Suggest_Icon_\(index)"))
        titleLabel.text = Localization("Suggest_\(index)")
    }
}
