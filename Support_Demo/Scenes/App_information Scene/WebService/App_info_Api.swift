//
//  App_info_Api.swift
//  Support_Demo
//
//  Created by Macbook on 17/04/2021.
//

import Foundation
 
class App_info_Api {
    private var AppStore_info : App_info?
    func get_AppStore_info() {
        let UrlString = "https://itunes.apple.com/lookup?id=361309726"
        guard let Api_Url = URL(string: UrlString) else {
            return
        }
        let Request = URLRequest(url: Api_Url)
        let task = URLSession.shared.dataTask(with: Request) { (data, response, error) in
            if let error = error {
                print(error)
                return
            }
            if let data = data {
                let decoder = JSONDecoder()
                if let jsonResults = try? decoder.decode(App_info.self, from: data){
                    self.AppStore_info = jsonResults
                       }
                OperationQueue.main.addOperation {
                   
                }}}
        task.resume()
    }
}
