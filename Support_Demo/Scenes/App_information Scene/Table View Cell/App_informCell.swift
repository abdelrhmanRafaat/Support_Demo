//
//  App_informCell.swift
//  Support_Demo
//
//  Created by Macbook on 10/04/2021.
//

import UIKit

class App_informCell: UITableViewCell {
    @IBOutlet weak var AppInformationCell : UILabel!
    @IBOutlet weak var AppVersion : UILabel!
    @IBOutlet weak var SepratorView: UIView!
    var result : Results?
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func ConfigureCell(_ indexPath : IndexPath,_ AppStoreVersion : String) {
        AppInformationCell.text = Localization("App_Inform_\(indexPath.row)")
        switch indexPath.row {
        case 0:
            AppVersion.text = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        case 1:
            AppVersion.text = AppStoreVersion
        default:
            SepratorView.isHidden = true
            accessoryType = .disclosureIndicator
            AppVersion.text = ""
        }}}
