//
//  AppInformationViewController.swift
//  Support_Demo
//
//  Created by Macbook on 12/04/2021.
//

import UIKit

class AppInformationViewController: UITableViewController {
   
    var AppStore_info : App_info?
    var AppStore_Version : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = Localization("setting_1")
        self.tableView.registerNIB(cell: App_informCell.self)
        tableView.separatorStyle = .none
        get_AppStore_info(AppBundle: "361309726")
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width , height: 40.0)) //set these values as necessary
        returnedView.backgroundColor = .systemGray6
        return returnedView
            }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue() as App_informCell
        // Configure the cell...
        cell.ConfigureCell(indexPath, AppStore_Version ?? "")
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 2 {
        if checkUpdate(Current_appVersion: Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String, AppStore_Version: AppStore_Version){
            Update_Alert()
        }}}}
extension AppInformationViewController {
    func get_AppStore_info(AppBundle : String) {
        let UrlString = "https://itunes.apple.com/lookup?id=\(AppBundle)"
        guard let Api_Url = URL(string: UrlString) else {
            return
        }
        let Request = URLRequest(url: Api_Url)
        let task = URLSession.shared.dataTask(with: Request) { (data, response, error) in
            if let error = error {
                print(error)
                return
            }
            if let data = data {
                let decoder = JSONDecoder()
                if let jsonResults = try? decoder.decode(App_info.self, from: data){
                    self.AppStore_info = jsonResults
                       }
                OperationQueue.main.addOperation {
                    self.AppStore_Version = self.AppStore_info?.results?[0].version
                    self.ConfigureApp_info(self.AppStore_info?.results?[0].version ?? "")
                    self.tableView.reloadData()
                }}}
        task.resume()
    }
}
 
