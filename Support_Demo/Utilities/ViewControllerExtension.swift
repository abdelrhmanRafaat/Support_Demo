//
//  ViewControllerExtension.swift
//  Support_Demo
//
//  Created by Macbook on 05/04/2021.
//

import UIKit


extension UIViewController {
        internal func Localization(_ LocalizedString : String) -> String {
        return LocalizationSystem.sharedInstance.localizedStringForKey(key: LocalizedString, comment: "")
    }
    
    internal func Update_Alert() {
        let alertController = UIAlertController(title: Localization("Update Available"), message: "", preferredStyle: .alert)
        let NextTime = UIAlertAction(title: Localization("Next time"), style: .cancel, handler: nil)
        let Skip = UIAlertAction(title: Localization("Skip this version" ), style: .destructive, handler: nil)
        let Update = UIAlertAction(title: "Update", style: .default, handler: nil)
        alertController.addAction(NextTime)
        alertController.addAction(Skip)
        alertController.addAction(Update)
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    @objc func popup() -> Void {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func Go_to_Setting() -> Void {
        self.performSegue(withIdentifier: "Go_To_Settings", sender: self)
    }
    internal func setupSuggestLeftBtn() -> Void {
            let image = UIImage(systemName: "xmark")?.withRenderingMode(.alwaysTemplate)
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(popup))
    }
    
    internal func setupSuggestRightBtn() -> Void {
            let image = UIImage(systemName: "gearshape.fill")?.withRenderingMode(.alwaysTemplate)
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(Go_to_Setting))
            self.navigationItem.rightBarButtonItem?.tintColor = UIColor(named: "NavigationButtonItems")
    }
    enum cellState {
        case colapse
        case expand
    }
    enum phoneSize {
        case Small
        case Medium
        case Large
    }
    func getPhoneSize(_ width: CGFloat) -> phoneSize {
        if width == 320 {
            return .Small
        }
        else if width >= 400 {
            return .Large
        }
        else {
            return .Medium
        }}
    func changeLang() {
        if LocalizationSystem.sharedInstance.getLanguage() == "en" {
            UIView.appearance().semanticContentAttribute = .forceLeftToRight
        }
        else {
            UIView.appearance().semanticContentAttribute = .forceRightToLeft
        }}
    func getLang () -> String {
        return LocalizationSystem.sharedInstance.getLanguage()
    }
    func Reload(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if #available(iOS 13.0, *) {
            if let delegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate {
                delegate.window?.rootViewController = storyboard.instantiateInitialViewController()
            }
        }else {
        if let delegate = UIApplication.shared.delegate as? AppDelegate {
                delegate.window?.rootViewController = storyboard.instantiateInitialViewController()
        }}}
    
    func ScreenShot () -> UIImage {
          return TakenScreenShot()
    }
    func TakenScreenShot() -> UIImage {
        
        //begin
        UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, false, UIScreen.main.scale)
        view.drawHierarchy(in: self.view.bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        if image != nil {
            return image!
        }
        return UIImage().withTintColor(.blue)
    }
    func ChangeAppViewMode(_ DarkMode : Bool) {
        let defaults = UserDefaults.standard
        switch DarkMode {
        case false:
            defaults.set(true, forKey: "Dark_Mode")
            UIApplication.shared.windows.forEach { window in
                window.overrideUserInterfaceStyle = .dark
            }
        default:
            defaults.set(false, forKey: "Dark_Mode")
            UIApplication.shared.windows.forEach { window in
                window.overrideUserInterfaceStyle = .light
            }}}
    func exitApp(){
        UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
             //Comment if you want to minimise app
             Timer.scheduledTimer(withTimeInterval: 0.2, repeats: false) { (timer) in
                 exit(0)
             }}
    func ConfigureApp_info(_ AppStoreVersion : String) {
        let defaults = UserDefaults.standard
        defaults.set(Bundle.main.infoDictionary?["CFBundleShortVersionString"] as! String, forKey: "Current_App_Version")
        defaults.set(AppStoreVersion, forKey: "App_Strore_Version")
    }
    func ChangeAppIcon() {
        let IconName = UIApplication.shared.alternateIconName ?? "Primary"
        if IconName == "Primary" {
            UIApplication.shared.setAlternateIconName("Black")
        }
        else {
            UIApplication.shared.setAlternateIconName(nil)
        }
        exitApp()
    }
    
    func checkUpdate(Current_appVersion : String, AppStore_Version : String) -> Bool {
        if Current_appVersion < AppStore_Version {
           return true
        }
        return false
    }
    
    
    func TableViewAnimation() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)
    }}

extension UITableViewCell {
    internal func Localization(_ LocalizedString : String) -> String {
        return LocalizationSystem.sharedInstance.localizedStringForKey(key: LocalizedString, comment: "")
    }}

