//
//  TextFieldExtension.swift
//  Support_Demo
//
//  Created by Macbook on 10/04/2021.
//

import UIKit

class  CustomTextField : UITextField {
    
    var padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
    
    override func layoutSubviews() {
        if LocalizationSystem.sharedInstance.getLanguage() == "en" {
            self.textAlignment = .left
        }
        else {
            self.textAlignment = .right
        }
        super.layoutSubviews()
    }
    
    func setpadding(right : CGFloat,left : CGFloat) {
        padding = UIEdgeInsets(top: 0, left: left, bottom: 0, right: right)
    }
   override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
   override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

}
